<?php
return array(
    /**
     * 数据库配置
     *   */
    'DB'=>array(
        'db_type'       =>'MYSQL',//数据库类型
        'db_host'       =>'127.0.0.1',//地址
        'db_username'   =>'root',//账户
        'db_password'   =>'',//密码
        'db_prot'       =>'3306',//端口
        'db_name'       =>'dbname'//选中的数据库
    ),
);